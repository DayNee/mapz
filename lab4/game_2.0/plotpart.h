#pragma once
#include <string>
#include <list>
#include <fstream>
using namespace std;

class PlotPart
{
protected:
	string path;
	list<string> subparts;
public:
	PlotPart();
	PlotPart(const string&);
	const list<string>& getSubparts() const;
};

