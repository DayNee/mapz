#pragma once
#include "phrasenode.h"
#include <stack>
//#include "gameprocess.h"

class dialogueiterator
{
protected:
	PhraseNode* node;
	//map<string, PhraseNode*> collection;
	//map<string, PhraseNode*>::iterator it;
	//dialogueiterator() { this->gpm = GameProcess::getGP(); };
	//GameProcessMediator* gpm;
public:
	virtual dialogueiterator* _this() = 0;
	PhraseNode* operator *()
	{
		return this->node;
	}
};

class user_iterator : public dialogueiterator
{
private:

public:
	user_iterator(PhraseNode* n) { this->node = n;  }
	dialogueiterator* _next(const string& key)
	{
		if (this->node->children[key] != nullptr)
		{
			this->node->parent = this->node;
			this->node = this->node->children[key];
			//this->gpm->notify("Moved forvard");
			return this;
		}
	}
	dialogueiterator* _this() override { return this; }
	friend class GameProcess;
};

class lr_iterator : public dialogueiterator
{
	stack<int> cnts;
public:
	lr_iterator(PhraseNode* n) { this->node = n; cnts.push(0); };

	dialogueiterator* _next()
	{
		if (!this->node->children.empty())
		{
			if (this->cnts.top() < this->node->children.size())
			{
				this->node = std::next(this->node->children.begin(), cnts.top())->second;
				int top = cnts.top(); cnts.pop(); cnts.push(top + 1);
				cnts.push(0);
				//this->gpm->notify("Moved forvard");
				return this;
			}
		}
		if (this->node->parent != nullptr)
		{
			cnts.pop();

			this->node = this->node->parent;
			return this->_next();
		}
		else return nullptr;
	}
	dialogueiterator* _this()
	{
		return this;
	}	
};
