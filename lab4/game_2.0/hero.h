#pragma once
#include <list>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class Thing {};
class Hero
{
private:
	string name;
	int totalPoints;
	int freePoints;
	int smartness;
	int strength;
	int charisma;
	static void serialize(const Hero&);
	static Hero& deserialize(const string&);
	list<Thing> things;
public:
	Hero() {};
	Hero(const string&);
	Hero& operator = (const Hero&);
	string repr();
	friend class HeroController;
	friend class OppMediator;
	friend class Configuration;
	friend class Type;
};

