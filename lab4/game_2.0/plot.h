#pragma once
#include <string>
#include <list>
#include <fstream>
using namespace std;
#include "plotpartdecorator.h"
#include "level.h"


class Plot : public PlotPartDecorator
{
private:
	list<string>::iterator level;
	Level* lvl = nullptr;
public:
	Plot(const string&);
	void nextLevel();
	const string& getLevel() const;
};

