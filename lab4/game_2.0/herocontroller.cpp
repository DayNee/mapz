#include "herocontroller.h"

void HeroController::takeHero(Hero* h, Type* t)
{
	this->hero = h;
	this->type = t;
}
#include <iostream>
void HeroController::upPoints(const int& p)
{
	this->hero->freePoints += p;
	this->hero->totalPoints += p;
	std::cout << "upPoints : points upped to " << this->hero->freePoints <<endl;
}

void HeroController::upSmartness(const int& smp)
{
	if (smp <= this->hero->freePoints)
	{
		this->hero->smartness += smp;
		this->hero->freePoints -= smp;
	}
}

void HeroController::upStrength(const int& stp)
{
	if (stp <= this->hero->freePoints)
	{
		this->hero->strength += stp;
		this->hero->freePoints -= stp;
	}
}

void HeroController::upCharisma(const int& chp)
{
	if (chp <= this->hero->freePoints)
	{
		this->hero->charisma += chp;
		this->hero->freePoints -= chp;
	}
}

void HeroController::fight()
{
	this->type->fight();
}

void HeroController::talk()
{
	this->type->talk();
}

void HeroController::thinkof()
{
	this->type->thinkof();
}
