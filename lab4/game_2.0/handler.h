#pragma once
#include "configuration.h"
#include "herocontroller.h"
class Handler
{
protected:
	Configuration* cnf;
public:
	Handler(Configuration* cnf) { this->cnf = cnf; }
	virtual Configuration* handle() = 0;
};

class HeroHandler : public Handler
{
public:
	HeroHandler(Configuration* cnf) : Handler(cnf) {}
	Configuration* handle();
};

class PlotHandler : public Handler
{
public:
	PlotHandler(Configuration* cnf) : Handler(cnf) {}
	Configuration* handle();
};