#pragma once
#include <list>
#include <string>
using namespace std;
#include "plotpartdecorator.h"
#include "phrasenode.h"

class Level : public PlotPartDecorator
{
private:
	PhraseNode* node;
public:
	Level(const string&);
};

