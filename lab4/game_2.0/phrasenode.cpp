#include "phrasenode.h"

PhraseNode::PhraseNode(const PhraseNode& nd)
{
	this->phrase = nd.phrase;
	this->parent = nd.parent;
	this->children = nd.children;
}

PhraseNode::PhraseNode(const string& title, const list<string>& variants)
{
	this->phrase = title;
	for (auto it = variants.begin(); it != variants.end(); it++)
	{
		this->children.insert({ *it, nullptr });
	}
}

string PhraseNode::getPhrase()
{
	return this->phrase;
}

map<string, PhraseNode*> PhraseNode::getChildren()
{
	return this->children;
}

PhraseNode* PhraseNode::getParent()
{
	return this->parent;
}

string PhraseNode::repr() const
{
	string str = "";
	str += this->phrase + "\n";
	for (auto it = this->children.begin(); it != this->children.end(); it++)
	{
		str += "\t" + it->first + "\n";
	}
	return str;
}
