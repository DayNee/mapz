﻿#include <iostream>
using namespace std;
#include "configuration.h"
#include "gameprocess.h"
#include "dialogue.h"
#include "dialogueparser.h"
#include "dialogueiterator.h"


int main()
{
	GameProcess* gp = GameProcess::getGP();
	gp->init("smart");
	gp->play();
	return 0;
}