#pragma once
#include <string>
#include <fstream>
#include "phrasenode.h"
#include "hero.h"
#include "herocontroller.h"

#include "dialogue.h"
#include "dialogueparser.h"
#include "dialogueiterator.h"
using namespace std;

class Configuration
{
private:
	Hero hero;
	PhraseNode* phnd;
	static void serialize(const Configuration&);
	
public:
	static Configuration& deserialize(const string&);
	
	Configuration() {};
	Configuration(const Hero&, PhraseNode*);
	Configuration(const Configuration&);
	Hero* getHero();
	PhraseNode* getNode();
	void setNode(PhraseNode* node) { this->phnd = node; }
	Configuration& operator = (const Configuration&);
	friend class GameProcess;
};

