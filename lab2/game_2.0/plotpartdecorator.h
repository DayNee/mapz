#pragma once
#include "plotpart.h"

class PlotPartDecorator : public PlotPart
{
protected:
	PlotPart* pp;
public:
	PlotPartDecorator(const string&);
};

