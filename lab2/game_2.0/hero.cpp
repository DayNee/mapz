#include "hero.h"

void Hero::serialize(const Hero& hero)
{
	ofstream out(hero.name + ".txt");
	out << "@hero" << endl;
	out << hero.name << endl << hero.totalPoints << endl << hero.freePoints << endl <<
		hero.smartness << endl << hero.strength << endl << hero.charisma <<endl;
	out.close();
}

Hero& Hero::deserialize(const string& name)
{
	ifstream fin(name + ".txt");
	list<string> lines;
	string line;
	if (fin.is_open())
	{
		while (true) 
		{
			getline(fin, line);
			if (fin.eof()) break;
			lines.push_back(line);
		}
		if (lines.size() == 7)
		{
			Hero* hero = new Hero;
			auto it = lines.begin();
			if (*it == "@hero")
			{
				try
				{
					hero->name = *next(it);
					//cout << hero->name;
					//cout << *next(it);
					hero->totalPoints = stoi(*next(it, 2));
					hero->freePoints = stoi(*next(it, 3));
					hero->smartness = stoi(*next(it, 4));
					hero->strength = stoi(*next(it, 5));
					hero->charisma = stoi(*next(it, 6));
					return *hero;
				}
				catch (exception ex)
				{
					cout << ex.what();
				}
			}
		}
	}
}

Hero::Hero(const string& name)
{
	this->name = name;
	this->freePoints = 0;
	this->smartness = 1;
	this->strength = 1;
	this->charisma = 1;
	this->totalPoints = 3;
}

Hero& Hero::operator=(const Hero& l)
{
	if (this != &l)
	{
		this->name = l.name;
		this->freePoints = l.freePoints;
		this->totalPoints = l.totalPoints;
		this->charisma = l.charisma;
		this->smartness = l.smartness;
		this->strength = l.strength;
		this->things = l.things;
	}
	return *this;
}

string Hero::repr()
{
	return to_string(this->freePoints);
}
