#include "dialogue.h"

PhraseNode* Dialogue::getNode()
{
	return this->startNode;
}

string Dialogue::repr()
{
	return this->detour(this->startNode, 0);
}

lr_iterator Dialogue::getLRIt()
{
	return lr_iterator(this->startNode);
}

user_iterator Dialogue::getUserIt()
{
	return user_iterator(this->startNode);
}

string Dialogue::detour(PhraseNode* node, const int depth)
{
	string str, spaces;
	for (int i = 0; i < depth; i++)
		spaces += ' ';
	str += spaces + node->getPhrase() + "\n";
	for (auto it = node->children.begin(); it != node->children.end(); it++)
	{
		str += spaces + it->first + "\n";
		str += this->detour(it->second, depth + 3);
	}
	return str;
}
