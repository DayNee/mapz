﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Core
{
    class BracketsExpression : IExpression
    {
        private string name;
        public int Priority { get { return 1; } }
        public BracketsExpression(string _name)
        {
            name = _name;
        }
        public dynamic Solve(Context context)
        {
            Interpreter interpreter = new Interpreter();
            interpreter.Execute(context.Brackets[name], context, name);
            return "buffer";
        }
    }
}
