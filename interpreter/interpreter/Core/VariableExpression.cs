﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Core
{
    class VariableExpression: IExpression
    {
        public int Priority { get { return 5; } }
        public dynamic Solve(Context context)
        {
            if (!context.Memory.Keys.Contains(name))
            {
                context.Memory.Add(name, "");
            }
            return name;
        }
        public VariableExpression(string _name) 
        {
            name = _name;
        }
        private string name;
        
        
    }
}
