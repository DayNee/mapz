﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Core
{
    class BlockExpression:IExpression
    {
        public int Priority { get { return 1; } }
        private string name;
        public BlockExpression(string _name)
        {
            name = _name;
        }
        public dynamic Solve(Context context) 
        {
            Interpreter interpreter = new Interpreter();
            interpreter.Execute(context.Blocks[name], context, name);
            return null;
        }
    }
}
