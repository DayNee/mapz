﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Core
{
    abstract class UnaryExpression: IExpression
    {
        abstract public int Priority { get; }
        public IExpression Op1 { get; set; }
        abstract public dynamic Solve(Context context);
    }
}
