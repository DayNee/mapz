﻿Створення змінної або присвоєння їй іншого значення(так змінна створюється автоматично при першому використанні):
    x = 4
    x = x - 5
Вивід в консоль:
    Print x
Умовний оператор if:
    x = 1
    if ( x > 4 )
    {
        x = 5
    }
Цикл while:
    i = 25
    x = 0
    while ( i != 0 )
    {
        i = i - 1
        x = x + i
    }
Створення файлу:
    CreateFile "D:\test.txt"
Створення бекапу:
    BackupFile "D:\test.txt"
Видалення файлу:
    DeleteFile "D:\test.txt"
Пошук файлів, що повторюються:
    FindDuplicateFile "D:\"
Копіювання файлу:
    CopyFile "D:\test.txt" "D:\testCopy.txt"
Пошук файлу за маскою:
    FindFileByMask "D:\test" "te*.*"
Переміщення файлу:
    MoveFile "D:\test.txt" "D:\test\"