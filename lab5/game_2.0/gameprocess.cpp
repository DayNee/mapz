﻿#include "gameprocess.h"
#define pause system("pause");

GameProcess* GameProcess::gp = nullptr;

void GameProcess::promisciousSave()
{
	this->config.setNode(this->it->operator*());
	this->configs.push(this->config);
	this->config = Configuration(this->config);
}

GameProcess* GameProcess::getGP()
{
	if (GameProcess::gp == nullptr)
		GameProcess::gp = new GameProcess;
	else return GameProcess::gp;
}

void GameProcess::init(string t)
{
	this->config = Configuration::deserialize("Oleh");
	this->it = new user_iterator(this->config.phnd);
	if (t == "strong")
		this->hm.takeHero(this->config.getHero(), new Strong());
	else if (t == "smart")
		this->hm.takeHero(this->config.getHero(), new Smart());
	else if (t == "charm")
		this->hm.takeHero(this->config.getHero(), new Charismatic());
}


#include "dialogue.h"
#include "dialogueparser.h"
#include <iostream>

void GameProcess::play()
{
	try {

		string str;
		while (true)
		{
			system("cls");
			if (it->operator*() != nullptr)
			{

				cout << it->operator*()->repr();
				if (it->operator*()->getChildren().empty()) break;
				cin >> str;
				bool f = 0;
				if (str == "save")
				{
					cout << "\rsaved" <<endl;
					this->promisciousSave();
					f = 1;
				}
				else if (str == "back")
				{
					this->back();
					f = 1;
					pause
				}
				else if (str == "fight")
				{
					this->hm.fight();
					system("pause");
					f = 1;
				}
				if (!f)
				{
					auto ch = it->operator*()->getChildren();
					if (ch.find(str) != ch.end())
					{
						it->_next(str);
						this->hm.upPoints(1);
						pause
					}
					else cout << "wrong ans" << endl;
				}
//				
			}
			else break;
		}
		this->save();
		while (!this->configs.empty())
		{
			cout << this->configs.top().getHero()->repr() << " " << this->configs.top().getNode()->getPhrase() << endl ;
			this->configs.pop();
		}
	}
	catch (exception ex)
	{
		cout << ex.what() << endl;
	}
}

void GameProcess::back()
{
	if (this->configs.size() > 1)
	{
		cout << this->config.getNode()->getPhrase() <<   endl;
		this->configs.pop();
		this->config = this->configs.top();
		cout << this->config.getNode()->getPhrase() << endl;
		this->it->node = this->config.getNode();
		this->hm.takeHero(config.getHero(), this->hm.getType());
	}
}

void GameProcess::save()
{
	Configuration::serialize(this->config
	);
}

string GameProcess::notify(const string&)
{
	return 0;
}

