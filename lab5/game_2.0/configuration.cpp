#include "configuration.h"

Configuration::Configuration(const Configuration& obj)
{
	this->hero = obj.hero;
	this->phnd = obj.phnd;

}

void Configuration::serialize(const Configuration& obj)
{
	Hero::serialize(obj.hero);
	ofstream out(obj.hero.name + ".txt", ios::app);
	out << obj.phnd->getPhrase();
}

Configuration& Configuration::deserialize(const string& name)
{
	list<Dialogue*> dl = DialogueParser::getParser().parseDialogues("script.txt");
	auto df = *dl.begin();
	Hero hero(Hero::deserialize(name));
	ifstream fin(name + ".txt");
	string str;
	for (int i = 0; i < 8; i++)
		getline(fin, str);
	//cout << str;
	auto it = df->getLRIt();
	while (true)
	{
		if (it._this()->operator*()->getPhrase() == str)
		{
			cout << "is" << endl;
			break;
		}
		cout << it._next()->operator*()->getPhrase() <<endl;
		//it._next();
		//cout << (*it)->getPhrase() << endl;
	}
	return *new Configuration(hero, it.operator*());
}

Configuration::Configuration(const Hero& hero, PhraseNode* nd)
{
	this->hero = hero;
	this->phnd = nd;
}

Hero* Configuration::getHero()
{
	return &this->hero;
}

PhraseNode* Configuration::getNode()
{
	return this->phnd;
}

Configuration& Configuration::operator=(const Configuration& cnf)
{
	if (this != &cnf)
	{
		this->hero = cnf.hero;
		this->phnd = cnf.phnd;
	}
	return *this;
}
