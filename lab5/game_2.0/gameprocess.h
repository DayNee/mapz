#pragma once
#include <exception>
#include <stack>
using namespace std;
#include "configuration.h"
#include "dialogueiterator.h"
#include "handler.h"

class GameProcess 
{
private:
	static GameProcess* gp;
	stack<Configuration> configs;
	Configuration config;
	user_iterator* it;
	HeroController hm;
	void promisciousSave();
	GameProcess() {};
public:
	//get n set
	static GameProcess* getGP();
	void init(string t);
	void play();
	void back();
	void save();
	// TO DO:
	//moments 
	// ADD CONROLLERS FOR CHARACTER AND PLOT INTERACTION
};

