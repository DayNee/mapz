#include "dialogueparser.h"

DialogueParser* DialogueParser::parser = nullptr;

DialogueParser& DialogueParser::getParser()
{
	if (parser == nullptr)
		parser = new DialogueParser;
	return *parser;
}

string DialogueParser::read()
{
	string line = "";
	if (!fin.eof())getline(fin, line);
	return line;
}

PhraseNode* DialogueParser::parseTree(int d, PhraseNode* prnt)
{
	if (fin.eof()) return nullptr;
	string line;
	getline(fin, line);
	line = rstrip(line);
	if (line == "<") return nullptr;
	PhraseNode* nd = new PhraseNode(line, prnt);
	nd->parent = prnt;
	//cout << "phrase\t" << d << "\t" << line << endl;
	while(true)
	{
		if (fin.eof()) break;
		getline(fin, line);
		line = rstrip(line);
		if (line[0] != '>') break;
//		cout << "reply\t" << d<<"\t" <<line << endl;
		PhraseNode*  ptr = this->parseTree(d+1, nd);
		nd->children.insert({ line, ptr });
	}
	return nd;
}

list<Dialogue*> DialogueParser::parseDialogues(const string& path)
{
	this->fin.open(path);
	if (!fin.is_open()) throw exception("cannot open plot file");
	list<Dialogue*> nodes;
	Dialogue* hnd;
	PhraseNode* nd;
	string line;
	while (true)
	{
		if (!fin.eof() || line[0] != '@')getline(fin, line);
		else break;
		hnd = new Dialogue();
		hnd->title = rstrip(line);
		hnd->startNode = this->parseTree(0, nullptr);
		if (hnd->startNode == nullptr) break;
		nodes.push_back(hnd);
	}
	return nodes;
}

int spaceCounter(const string& str)
{
	int counter = 0;
	while (str[counter] == ' ')
	{
		counter++;
	}
	return counter;
}

string rstrip(const string& str)
{
	string newStr;
	if (str.length() != 0) {
		int b = 0, e = str.length() - 1;
		while (str[b] == ' ' || str[b] == '\t')
			b++;
		while (str[e] == ' ' || str[e] == '\t' || str[e] == '\n')
			e--;
		newStr = str.substr(b, e+1);
	}
	return newStr;
}