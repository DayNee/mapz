#pragma once
#include "hero.h"
#include "gameprocessmediator.h"
#include "gameprocess.h"


class Type 
{
public:
	virtual string fight() = 0;
	virtual string talk() = 0;
	virtual string thinkof() = 0;
};

class Strong : public Type
{
public:
	string fight() override { return "successful fight\n"; };
	string talk() override { return  "unsuccessful talk\n"; };
	string thinkof() override { return "unsuccessful thinking\n"; };
};

class Smart : public Type
{
public:
	string fight() override { return "unsuccessful fight\n"; };
	string talk() override { return  "unsuccessful talk\n"; };
	string thinkof() override { return "successful thinking\n"; };
};

class Charismatic : public Type
{
public:
	string fight() override { return "unsuccessful fight\n"; };
	string talk() override { return  "successful talk\n"; };
	string thinkof() override { return "unsuccessful thinking\n"; };
};


class HeroController
{
private:
	Hero* hero = nullptr;
	Type* type;
	
public:
	Type* getType() { return this->type; };
	void takeHero(Hero*, Type*);
	void upPoints(const int&);
	void upSmartness(const int&);
	void upStrength(const int&);
	void upCharisma(const int&);
	void fight();
	void talk();
	void thinkof();
};

