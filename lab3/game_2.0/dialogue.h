#pragma once
#include "phrasenode.h"
#include "dialogueiterator.h"

class dialogueiterator;
class Dialogue
{
protected:
	string title;
	PhraseNode* startNode;
	string detour(PhraseNode*, const int);
public:
	Dialogue() {};
	PhraseNode* getNode();
	string repr();
	lr_iterator getLRIt();
	user_iterator getUserIt();
	friend class DialogueParser;
};

