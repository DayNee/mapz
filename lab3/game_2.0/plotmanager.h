#pragma once
#include "plot.h"
#include "dialogue.h"
#include "phrasenode.h"
class PlotManager
{
private:
	Plot* plot;
	list<string>::iterator level;
	Level* currLevel;	
	PhraseNode* currNode;
	Level* deployLevel(const string&);
public:
	PlotManager(const PlotManager&);
	void nextLevel();
};

